<?php include 'header.php'; ?>
  <main>
    <div class="home-1">
      <h1 class="is-size-1">Scanderé</h1>
      <h2 class="is-size-2-desktop is-size-3-touch">Arboriste Élagueur Grimpeur</h2>
      <div class="scroll-icon is-hidden-touch"></div>
    </div>
    <div class="home-2">
      <section class="tree is-hidden-touch">
        <div id="leaf-info-1" class="leaf-info animated">
          <span>Présentation</span>
        </div>

        <div id="leaf-info-2" class="leaf-info animated">
          <span>Définition</span>
          <p>
            SCANDO, IS, ERE, DI, SUM tr
            <br>
            Escalader v.t : franchir par escalade.
          </p>
        </div>

        <div id="leaf-info-3" class="leaf-info animated">
          <span>Zones d'intervention</span>
          <p>
            Scanderé est présente dans <strong>le Maine-et-Loire</strong>, <strong>la Vienne</strong> et <strong>les Deux-Sèvres</strong>.
          </p>
        </div>

        <div id="leaf-info-4" class="leaf-info animated">
          <span>L'Équipe</span>
          <p>
            Scanderé possède une équipe expérimentée qui offre un travail rigoureux dans les situations les plus délicates.
          </p>
           <p>
            Elle sollicite toutes solutions pour l’arbre et son environnement ainsi que pour les biens matériels qui l’entourent
          </p>
        </div>


        <div id="leaf-info-6" class="leaf-info animated">
          <span>Le Gérant</span>
          <p>
            Julien Laury gérant de Scanderé est Arboriste Élagueur Grimpeur diplômé d’état, et à mis toute son expérience dans l'entreprise.
          </p>
          <p>
            Aujourd'hui sa notoriété est faite grâce à la complexité de ses chantiers d'élagages et d'abattages délicats.
          </p>
        </div>


        <div id="leaf-info-8" class="leaf-info animated">
          <span>Interventions</span>
          <p>
            Scanderé intervient après des particuliers, des entreprises et des collectivités.
        </div>

        <div id="leaf-info-9" class="leaf-info animated">
          <span>Devis</span>
          <p>
            Scanderé effectue votre devis 48H après visite du chantier. Après retour de notre offre signée, nous vous contactons pour fixer ensemble une date qui vous convienne.
          </p>
        </div>

        <div id="leaf-info-10" class="leaf-info animated">
          <span>L'Équipe</span>
          <p>
            Scanderé possède une équipe expérimentée qui offre un travail rigoureux dans les situations les plus délicates.
          </p>
        </div>

        <div class="leaf-tree" id="leaf-tree-1">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-1" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#93b939" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-2">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-2" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#93b939" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-3">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-3" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#71892a" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-4">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-4" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#93b939" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-5">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-5" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#71892a" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-6">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-6" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#c1d17f" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-7">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-7" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#93b939" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-8">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-8" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#c1d17f" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-9">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-9" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#71892a" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>

        <div class="leaf-tree" id="leaf-tree-10">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.77 128.09">
            <g id="lines" fill="none" fill-rule="evenodd" stroke="currentColor" stroke-width="1">
                <path class="leaf-path-10" d="M75.27,85.74c-.08,23.18-16.88,41.92-37.53,41.85S.42,108.67.5,85.48,38.17,1,38.17,1,75.35,62.55,75.27,85.74Z" fill="none" stroke="#93b939" stroke-miterlimit="10"/>
            </g>
          </svg>
        </div>
      </section>
      <section class="accordions is-hidden-desktop">
        <div class="accordion">
          <div class="head">
            <h2 class="title">Présentation</h2>
            <i class="fas fa-caret-down"></i>
          </div>
          <div class="content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias eos delectus repellat aliquam consectetur non impedit minus, illo, rerum modi, voluptates magnam repellendus. Quam repellat magni vero vitae deserunt!</p>
          </div>
        </div>
        <div class="accordion">
          <div class="head">
            <h2 class="title">Définition</h2>
            <i class="fas fa-caret-down"></i>
          </div>
          <div class="content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias eos delectus repellat aliquam consectetur non impedit minus, illo, rerum modi, voluptates magnam repellendus. Quam repellat magni vero vitae deserunt!</p>
          </div>
        </div>
        <div class="accordion">
          <div class="head">
            <h2 class="title">L'équipe</h2>
            <i class="fas fa-caret-down"></i>
          </div>
          <div class="content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias eos delectus repellat aliquam consectetur non impedit minus, illo, rerum modi, voluptates magnam repellendus. Quam repellat magni vero vitae deserunt!</p>
          </div>
        </div>
        <div class="accordion">
          <div class="head">
            <h2 class="title">Zones d'intervention</h2>
            <i class="fas fa-caret-down"></i>
          </div>
          <div class="content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias eos delectus repellat aliquam consectetur non impedit minus, illo, rerum modi, voluptates magnam repellendus. Quam repellat magni vero vitae deserunt!</p>
          </div>
        </div>
        <div class="accordion">
          <div class="head">
            <h2 class="title">Le Gérant</h2>
            <i class="fas fa-caret-down"></i>
          </div>
          <div class="content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias eos delectus repellat aliquam consectetur non impedit minus, illo, rerum modi, voluptates magnam repellendus. Quam repellat magni vero vitae deserunt!</p>
          </div>
        </div>
      </section>
    </div>
  </main>
<?php include 'footer.php'; ?>
