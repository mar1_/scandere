// Navigation mobile
jQuery(document).ready(function ($) {
  $('.navbar-burger').click(function () {
    $('.navbar-burger').toggleClass('is-active');
    $('.navbar-menu').toggleClass('is-active');
  });
  $('.bloc-adress-desktop').click(function () {
    $('.text-overlay-desktop').toggleClass('show');
  });
});

// Animation feuilles en scroll
jQuery(document).ready(function ($) {

  var waypoint1 = new Waypoint({
    element: document.getElementById('leaf-tree-1'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-1').css({
        'visibility': 'visible'
      });

      var leaf1 = anime({
        targets: '#leaf-tree-1 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-1').css({
          fill: '#93b939',
          transition: '.5s'
        });
        $('#leaf-info-1').toggleClass('fadeIn');
        $('#leaf-info-1').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint2 = new Waypoint({
    element: document.getElementById('leaf-tree-2'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-2').css({
        'visibility': 'visible'
      });
      var leaf2 = anime({
        targets: '#leaf-tree-2 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-2').css({
          fill: '#93b939',
          transition: '.5s'
        });
        $('#leaf-info-2').toggleClass('fadeIn');
        $('#leaf-info-2').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    },
  });

  var waypoint3 = new Waypoint({
    element: document.getElementById('leaf-tree-3'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-3').css({
        'visibility': 'visible'
      });
      var leaf3 = anime({
        targets: '#leaf-tree-3 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-3').css({
          fill: '#71892a',
          transition: '.5s'
        });
        $('#leaf-info-3').toggleClass('fadeIn');
        $('#leaf-info-3').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });


  var waypoint4 = new Waypoint({
    element: document.getElementById('leaf-tree-4'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-4').css({
        'visibility': 'visible'
      });
      var leaf4 = anime({
        targets: '#leaf-tree-4 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-4').css({
          fill: '#93b939',
          transition: '.5s'
        });
        $('#leaf-info-4').toggleClass('fadeIn');
        $('#leaf-info-4').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint5 = new Waypoint({
    element: document.getElementById('leaf-tree-5'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-5').css({
        'visibility': 'visible'
      });
      var leaf5 = anime({
        targets: '#leaf-tree-5 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-5').css({
          fill: '#71892a',
          transition: '.5s'
        });
        $('#leaf-info-5').toggleClass('fadeIn');
        $('#leaf-info-5').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint6 = new Waypoint({
    element: document.getElementById('leaf-tree-6'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-6').css({
        'visibility': 'visible'
      });
      var leaf6 = anime({
        targets: '#leaf-tree-6 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-6').css({
          fill: '#c1d17f',
          transition: '.5s'
        });
        $('#leaf-info-6').toggleClass('fadeIn');
        $('#leaf-info-6').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint7 = new Waypoint({
    element: document.getElementById('leaf-tree-7'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-7').css({
        'visibility': 'visible'
      });
      var leaf7 = anime({
        targets: '#leaf-tree-7 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-7').css({
          fill: '#93b939',
          transition: '.5s'
        });
        $('#leaf-info-7').toggleClass('fadeIn');
        $('#leaf-info-7').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint8 = new Waypoint({
    element: document.getElementById('leaf-tree-8'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-8').css({
        'visibility': 'visible'
      });
      var leaf8 = anime({
        targets: '#leaf-tree-8 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-8').css({
          fill: '#c1d17f',
          transition: '.5s'
        });
        $('#leaf-info-8').toggleClass('fadeIn');
        $('#leaf-info-8').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint9 = new Waypoint({
    element: document.getElementById('leaf-tree-9'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-9').css({
        'visibility': 'visible'
      });
      var leaf9 = anime({
        targets: '#leaf-tree-9 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-9').css({
          fill: '#71892a',
          transition: '.5s'
        });
        $('#leaf-info-9').toggleClass('fadeIn');
        $('#leaf-info-9').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });

  var waypoint10 = new Waypoint({
    element: document.getElementById('leaf-tree-10'),
    offset: 300,
    handler: function (direction) {
      $('#leaf-tree-10').css({
        'visibility': 'visible'
      });
      var leaf10 = anime({
        targets: '#leaf-tree-10 #lines path',
        strokeDashoffset: [anime.setDashoffset, 0],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(function () {
        $('.leaf-path-10').css({
          fill: '#93b939',
          transition: '.5s'
        });
        $('#leaf-info-10').toggleClass('fadeIn');
        $('#leaf-info-10').css({
          'visibility': 'visible'
        });
      }, 1100);
      this.destroy()
    }
  });
});

// Accordéons
jQuery(document).ready(function ($) {
  $(".accordion").on("click", function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .find("svg")
        .removeClass("fa-caret-up")
        .addClass("fa-caret-down");
      $(this)
        .children(".content")
        .slideUp(200);
      $(".accordion > svg")
        .removeClass("fa-caret-up")
        .addClass("fa-caret-down");
    } else {
      $(".accordion > svg")
        .removeClass("fa-caret-up")
        .addClass("fa-caret-down");
      $(this).addClass("active");
      $(this)
        .find("svg")
        .removeClass("fa-caret-down")
        .addClass("fa-caret-up");
      $(this)
        .children(".content")
        .slideUp(200);
      $(this)
        .children(".content")
        .slideDown(200);
    }
  });
});