    <footer>
      <div class="footer-content columns">
        <div class="footer-menu is-hidden-touch column is-11">
          <svg class="footer-leaf" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.5 16.24">
            <g id="02f92734-cb28-4bb2-89bc-e0fc37b563cc">
              <path d="M9.09,16.24c-5,0-9.11-3.67-9.09-8.15S4.11,0,9.15,0,27.5,8.18,27.5,8.18,14.13,16.26,9.09,16.24Z" fill="#93b939"/>
            </g>
          </svg>
          <a id="home" class="navbar-item">
            Accueil
          </a>

          <svg class="footer-leaf" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.5 16.24">
            <g id="02f92734-cb28-4bb2-89bc-e0fc37b563cc">
              <path d="M9.09,16.24c-5,0-9.11-3.67-9.09-8.15S4.11,0,9.15,0,27.5,8.18,27.5,8.18,14.13,16.26,9.09,16.24Z" fill="#93b939"/>
            </g>
          </svg>
          <a class="navbar-item">
            Services
          </a>

          <svg class="footer-leaf" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.5 16.24">
            <g id="02f92734-cb28-4bb2-89bc-e0fc37b563cc">
              <path d="M9.09,16.24c-5,0-9.11-3.67-9.09-8.15S4.11,0,9.15,0,27.5,8.18,27.5,8.18,14.13,16.26,9.09,16.24Z" fill="#93b939"/>
            </g>
          </svg>
          <a class="navbar-item">
            Photos
          </a>

          <svg class="footer-leaf" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.5 16.24">
            <g id="02f92734-cb28-4bb2-89bc-e0fc37b563cc">
              <path d="M9.09,16.24c-5,0-9.11-3.67-9.09-8.15S4.11,0,9.15,0,27.5,8.18,27.5,8.18,14.13,16.26,9.09,16.24Z" fill="#93b939"/>
            </g>
          </svg>
          <a id="news" class="navbar-item">
            Actualités
          </a>

          <svg class="footer-leaf" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.5 16.24">
            <g id="02f92734-cb28-4bb2-89bc-e0fc37b563cc">
              <path d="M9.09,16.24c-5,0-9.11-3.67-9.09-8.15S4.11,0,9.15,0,27.5,8.18,27.5,8.18,14.13,16.26,9.09,16.24Z" fill="#93b939"/>
            </g>
          </svg>
          <a class="navbar-item">
            Contact
          </a>
        </div>
        <div class="footer-image is-hidden-touch column is-1"> 
          <img src="//localhost:3000/scandere/wp-content/uploads/2018/07/Fichier-1.png" alt="Logo GDH'Com">
        </div>
      </div>
      <div class="footer-info">
        <div class="footer-info-links">
          <a href="">Mentions Légales</a>
          <a href="">Copyright</a>
        </div>
        <div class="footer-info-text">
          <p>Copyright © Entreprise Scanderé 2018</p>
        </div>
      </div>
        <div class="footer-image is-hidden-desktop column is-12"> 
          <img src="//localhost:3000/scandere/wp-content/uploads/2018/07/Fichier-1.png" alt="Logo GDH'Com">
        </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
