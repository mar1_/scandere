<?php

// FAVICON
add_theme_support( 'custom-logo' );

function init_styles_and_scripts() {

  // CSS //
  wp_enqueue_style(
    'bulma',
    get_template_directory_uri() . '/node_modules/bulma/css/bulma.min.css',
    array(),
    '0.7.1',
    'all'
  );

  wp_enqueue_style(
    'animate',
    get_template_directory_uri() . '/node_modules/animate.css/animate.min.css',
    array(),
    '3.6.1',
    'all'
  );

  wp_enqueue_style(
    'theme-css',
    get_template_directory_uri() . '/css/main.css',
    array(),
    '1.0.0',
    'all'
  );


  // JS //
  wp_enqueue_script(
    'jquery',
    'https://code.jquery.com/jquery-3.3.1.min.js',
    array(),
    '3.3.1',
    true
  );

  wp_enqueue_script(
    'theme-js',
    get_template_directory_uri() . '/js/main.js',
    array(),
    '1.0.0',
    true
  );

  wp_enqueue_script(
    'font-awesome',
    'https://use.fontawesome.com/releases/v5.0.9/js/all.js',
    array(),
    '5.0.9',
    true
  );

  wp_enqueue_script(
    'anime',
    get_template_directory_uri() . '/node_modules/animejs/anime.min.js',
    array(),
    '2.2.0',
    true
  );

  wp_enqueue_script(
    'waypoints',
    get_template_directory_uri() . '/node_modules/waypoints/lib/jquery.waypoints.js',
    array(),
    '2.2.0',
    true
  );


}
add_action('wp_enqueue_scripts', 'init_styles_and_scripts');
