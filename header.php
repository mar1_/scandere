<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
  </head>
  <body>
  <nav class="navbar nav-mobile is-hidden-desktop" role="navigation" aria-label="main navigation">

    <div class="navbar-brand">

      <a class="navbar-item logo" href="https://bulma.io">
        <object class="logo-svg" type="" data="/scandere/wp-content/uploads/2018/05/Fichier-4-1.svg"></object>
      </a>

    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>

    </div>

    <div class="navbar-menu">
      <a class="navbar-item">
        Accueil
      </a>
      <a class="navbar-item">
        Services
      </a>
      <a class="navbar-item">
        Photos
      </a>
      <a class="navbar-item">
        Actualités
      </a>
      <a class="navbar-item">
        Contact
      </a>
    </div>
  </nav>
  <div class="columns bloc-contact-mobile is-hidden-desktop">
    <div class="column bloc-phone">
      06 66 45 33 22
    </div>
    <div class="column bloc-adress">
      <div class="column-is-one-third">
        <i class="fas fa-map-marker"></i>
      </div>
      <div class="column">
        <h2>Agence Maine-et-Loire</h2>
        <p>12 rue du Palna, 49 270 Le Puy Notre Dame</p>
        <br>
        <h2>Agence Deux-Sèvres</h2>
        <p>275 rue de la Jaunaie, 79290 Bouillé Loretz</p>
      </div>
    </div>
  </div>

  <nav class="navbar nav-desktop is-hidden-touch" role="navigation" aria-label="main navigation">

    <div class="navbar-brand">
      <a class="navbar-item logo" href="https://bulma.io">
        <object class="logo-svg" type="" data="/scandere/wp-content/uploads/2018/05/Fichier-4-1.svg"></object>
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div class="navbar-tagline">
      <strong>Scanderé</strong>  - Arboriste - Élagueur - Grimpeur
    </div>

    <div class="navbar-end">
      <div class="bloc-phone">
        06 66 45 33 22
      </div>
      <div class="bloc-adress-desktop columns">
          <i class="fas fa-map-marker"></i>
          <div class="text-overlay-desktop">
            <i class="fas fa-caret-up"></i>
            <h2 class="is-uppercase has-text-weight-bold">Agence Maine-et-Loire</h2>
            <p>12 rue du Palna, 49 270 Le Puy Notre Dame</p>
            <br>
            <h2 class="is-uppercase has-text-weight-bold">Agence Deux-Sèvres</h2>
            <p>275 rue de la Jaunaie, 79290 Bouillé Loretz</p>
          </div>

      </div>
    </div>
  </nav>


  <div class="navbar-menu-svg is-hidden-touch">
      <svg xmlns="http://www.w3.org/2000/svg" width="950" height="200" viewBox="0 0 216.28 58.34">
        <g id="5eb08600-511e-4705-8303-a51cf8d2ae3f">
          <path d="M23.73,1.6A16.86,16.86,0,1,1,38,32.15c-8.46,4-38-.85-38-.85S15.28,5.55,23.73,1.6Z" fill="#d5e090"/>
          <path d="M192.55,1.6a16.86,16.86,0,1,0-14.28,30.55c8.45,4,38-.85,38-.85S201,5.55,192.55,1.6Z" fill="#596927"/>
           <path class="leaf" id="leaf-1" href="google.fr" d="M36.14,15.89A16.86,16.86,0,1,1,63.91,35c-5.29,7.68-33.22,18.5-33.22,18.5S30.84,23.57,36.14,15.89Z" fill="#c1d17f"/>
           <path class="leaf" id="leaf-5" d="M181.17,15.89A16.86,16.86,0,1,0,153.4,35c5.29,7.68,33.21,18.5,33.21,18.5S186.46,23.57,181.17,15.89Z" fill="#657b29"/>
           <path class="leaf" id="leaf-4" d="M153.54,19.26a16.86,16.86,0,1,0-31.35,12.41c3.43,8.68,28.22,25.48,28.22,25.48S157,27.93,153.54,19.26Z" fill="#d77621"/>
           <path class="leaf" id="leaf-2" d="M62.14,19.26A16.86,16.86,0,1,1,93.5,31.67c-3.44,8.68-28.22,25.48-28.22,25.48S58.71,27.93,62.14,19.26Z" fill="#aec852"/>
           <path class="leaf" id="leaf-3" d="M90.09,24.13a16.86,16.86,0,1,1,33.72.27c-.07,9.34-17.14,33.94-17.14,33.94S90,33.46,90.09,24.13Z" fill="#93b938"/>
         </g>
     </svg>
  </div>
  <div class="leaf-desktop is-hidden-touch">
    <a id="home" class="navbar-item">
      Accueil
    </a>
    <a class="navbar-item">
      Services
    </a>
    <a class="navbar-item">
      Photos
    </a>
    <a id="news" class="navbar-item">
      Actualités
    </a>
    <a class="navbar-item">
      Contact
    </a>
  </div>
